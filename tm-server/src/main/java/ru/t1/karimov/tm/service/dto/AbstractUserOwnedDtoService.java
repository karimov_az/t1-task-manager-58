package ru.t1.karimov.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.karimov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.karimov.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.karimov.tm.dto.model.AbstractUserOwnedDtoModel;
import ru.t1.karimov.tm.exception.entity.EntityNotFoundException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.IndexIncorrectException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedDtoModel>
        extends AbstractDtoService<M> implements IUserOwnedDtoService<M> {

    @NotNull
    @Override
    protected abstract IUserOwnedDtoRepository<M> getRepository();

    @NotNull
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        @NotNull EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
            entityManager.getTransaction().begin();
            @NotNull final M result = repository.add(userId, model);
            entityManager.getTransaction().commit();
            return result;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(final @Nullable String userId, @Nullable final Comparator<M> comparator) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
            return repository.findAll(userId, comparator);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
            return repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException(ERROR_INDEX_OUT_OF_BOUNDS);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
            return repository.findOneByIndex(userId, index);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Long getSize(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
            entityManager.getTransaction().begin();
            repository.removeAll(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOne(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
            entityManager.getTransaction().begin();
            repository.removeOne(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
            entityManager.getTransaction().begin();
            repository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException(ERROR_INDEX_OUT_OF_BOUNDS);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
            entityManager.getTransaction().begin();
            repository.removeOneByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        @NotNull EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
            entityManager.getTransaction().begin();
            repository.update(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
