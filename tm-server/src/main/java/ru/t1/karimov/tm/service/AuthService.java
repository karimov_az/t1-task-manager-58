package ru.t1.karimov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.karimov.tm.api.service.IAuthService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.api.service.dto.ISessionDtoService;
import ru.t1.karimov.tm.api.service.dto.IUserDtoService;
import ru.t1.karimov.tm.dto.model.SessionDto;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.field.LoginEmptyException;
import ru.t1.karimov.tm.exception.field.PasswordEmptyException;
import ru.t1.karimov.tm.exception.user.AccessDeniedException;
import ru.t1.karimov.tm.exception.user.AuthenticationException;
import ru.t1.karimov.tm.util.CryptUtil;
import ru.t1.karimov.tm.util.HashUtil;

import java.util.Date;

@Getter
@Setter
@Service
@AllArgsConstructor
public final class AuthService implements IAuthService {

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ISessionDtoService sessionService;

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final UserDto user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDto session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private SessionDto createSession(@NotNull final UserDto user) throws Exception {
        @NotNull final SessionDto session = new SessionDto();
        @NotNull final String userId = user.getId();
        session.setUserId(userId);
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return sessionService.add(userId, session);
    }

    @NotNull
    @Override
    public UserDto check(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDto user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        if (user.getLocked()) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AuthenticationException();
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        return user;
    }

    @Override
    public void invalidate(@Nullable final SessionDto session) throws Exception {
        if (session == null) return;
        @NotNull final String userId = session.getUserId();
        sessionService.removeOne(userId, session);
    }

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) throws Exception {
        @NotNull final UserDto user = check(login, password);
        return getToken(user);
    }

    @NotNull
    @Override
    public UserDto registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDto validateToken(@Nullable final String token) {
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDto session = objectMapper.readValue(json, SessionDto.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        if (!sessionService.existsById(session.getUserId(), session.getId())) throw new AccessDeniedException();
        return session;
    }

}
