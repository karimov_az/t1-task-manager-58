package ru.t1.karimov.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.karimov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.karimov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.karimov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.karimov.tm.dto.model.TaskDto;
import ru.t1.karimov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.karimov.tm.exception.entity.TaskNotFoundException;
import ru.t1.karimov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.karimov.tm.exception.field.TaskIdEmptyException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.karimov.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    private EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }

    @NotNull
    private IProjectDtoRepository getProjectRepository() {
        return context.getBean(ProjectDtoRepository.class);
    }

    @NotNull
    private ITaskDtoRepository getTaskRepository() {
        return context.getBean(TaskDtoRepository.class);
    }

    @NotNull
    @Override
    public TaskDto bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
            @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final TaskDto task = Optional
                    .ofNullable(taskRepository.findOneById(userId, taskId))
                    .orElseThrow(TaskNotFoundException::new);
            task.setProjectId(projectId);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectDtoRepository projectRepository = getProjectRepository();
            @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
            @NotNull final List<TaskDto> tasks = taskRepository.findAllByProjectId(userId, projectId);
            entityManager.getTransaction().begin();
            for (@NotNull final TaskDto task : tasks) taskRepository.removeOneById(userId, task.getId());
            projectRepository.removeOneById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = getTaskRepository();
            @NotNull final TaskDto task = Optional
                    .ofNullable(taskRepository.findOneById(userId, taskId))
                    .orElseThrow(TaskNotFoundException::new);
            task.setProjectId(null);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
}
