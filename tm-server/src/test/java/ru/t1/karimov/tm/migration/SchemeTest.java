package ru.t1.karimov.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.karimov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public class SchemeTest extends AbstractSchemeTest {

    @Test
    public void test() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

}
